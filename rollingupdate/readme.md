## Rolling updates

1.  k apply -f service.yml

1. k apply -f ngix_deployment.yml --record
1. k get all

http://193.62.55.83:30123/

./curl_loop.sh

1. k rollout status deployment my-nginx
1. k rollout history deployment my-nginx
1. k rollout history deployment my-nginx --revision 1
1. k rollout history deployment my-nginx --revision=2

1. k rollout undo deployment my-nginx 
1. k rollout undo deployment my-nginx --to-revision=1
