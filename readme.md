## Basic K8s Config

###Pluralsight course
https://app.pluralsight.com/course-player?clipId=e4365e03-1344-4d64-bf4d-0977e05c5b8c

https://github.com/DanWahlin/DockerAndKubernetesCourseCode/blob/master/samples/deployments/nginx.deployment.yml

1. export KUBECONFIG=~/.kube/config:~/.kube/config_embassy.yml:~/.kube/hx-wp-webadmin-02_uniprot-admin.yml:~/.kube/hh-wp-webadmin-02_uniprot-admin.yml
1. kubectl config view --flatten > config_merged
1. kubectl auto complete and alias
   - source <(kubectl completion bash)
   - alias k=kubectl
   - complete -F __start_kubectl k


1. k config use-context embassy
1. k cluster-info
1. k get nodes


1. k get ns
1. kubectl create namespace demo
1. k config set-context --current --namespace=demo
1. k config view --minify
1. k config get-context

