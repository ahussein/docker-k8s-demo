## Basic StatefulSet

1. k apply -f ./
1. k get all


1. k describe pod my-nginx-0
1. k exec -it my-nginx-0 -- /bin/bash
1. hostname
1. apt update && apt install curl
1. curl  localhost
1. curl my-nginx-0
1. curl my-nginx-0.nginx-service-hs.demo.svc.cluster.local
1. curl my-nginx-1.nginx-service-hs.demo.svc.cluster.local


1. k scale statefulset my-nginx --replicas=5
1. k delete -f ./

http://193.62.55.83:30123/

http://hh-rke-wp-webadmin-02-worker-7.caas.ebi.ac.uk:30123


#####DNS
1. nginx-service-hs.demo.svc.cluster.local
1. my-nginx-0.nginx-service-hs.demo.svc.cluster.local
1. my-nginx-1.nginx-service-hs.demo.svc.cluster.local


#####PVC
1. k get pvc
1. k get pv
1. k exec -it my-nginx-0 -- /bin/bash
1. curl localhost
1. curl localhost
1. cat /var/log/nginx/access.log
1. cd /shared_for_all
