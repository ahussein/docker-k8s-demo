## Basic deployment

1. k create -f deploymnet.yml --save-config
1. k apply -f deploymnet.yml
1. k get deployments
1. k get replicasets
1. k get pods
1. k get all
1. k describe

1. k logs pod/my-nginx-5c59cb9945-vfb5p
1. k exec my-nginx-5c59cb9945-vfb5p -- pwd
1. k exec my-nginx-5c59cb9945-vfb5p -- bash -c 'pwd; ls'; 
1. k exec -it my-nginx-5c59cb9945-vfb5p -- /bin/bash
1. curl http://localhost:80

1. k delete pod

1. k scale deployment my-nginx --replicas=5
1. k scale deployment my-nginx --replicas=1

1. k apply -f service.yml
1. k get service

    http://193.62.55.83:30123/

1. k apply -f ./
1. k delete -f deploymnet.yml
1. k delete -f ./

