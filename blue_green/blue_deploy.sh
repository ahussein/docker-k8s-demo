#!/bin/bash

export TARGET_ROLE=blue
export IMAGE_VERSION=guoyingqi/nginx-blue
export ENV=test
export NODE_PORT=30121

# To run deployment:
cat nginx.deployment.yml | sh config.sh | kubectl apply -f -
cat nginx.service.yml | sh config.sh | kubectl apply -f -

# http://193.62.55.83:30121/
